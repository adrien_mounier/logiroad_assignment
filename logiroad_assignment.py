import os
THIS_FOLDER = os.path.dirname(os.path.abspath('Desktop\Exo introductif Logiroad\code_python.py'))

from PIL import Image, ImageFont, ImageDraw, ImageEnhance

# Ouverture du fichier texte "annotations"
bounding_boxes_file = open(os.path.join(THIS_FOLDER,'annotations.txt'))

# La fonction suivante extrait les données du fichier texte "annotations" et les classe dans un format adéquat. Le résultat de la fonction est une liste dont chaque élément est une liste correspondant à une ligne du fichier texte. Chaque élément de chaque sous-liste est un mot de la ligne correspondante.
def bounding_boxes_per_image():
    boxes = bounding_boxes_file.read().splitlines()
    new_boxes = []
    for box in boxes:
        new_box = []
        l = len(box)
        incr = 0
        while incr < l:
            mot = ''
            while incr < l and box[incr] != ' ':
                mot += box[incr]
                incr += 1
            new_box.append(mot)
            incr += 1
        new_boxes.append(new_box)
    return(new_boxes)

images_boxes = bounding_boxes_per_image()

# La fonction suivante prend en argument une liste de la forme ['image_name', 'number_of_boxes', '12','32','654', '526', ...] et renvoie une liste de listes dont chaque sous-liste est de la forme [(12,32),(654,526)]. Chaque sous-liste du résultat correspond respectivement aux coordonnées des points supérieur-gauche et inférieur-droit de la boîte englobante qu'elle représente.
def create_boxes(image):
    boxes = []
    number_of_boxes = int(image[1])
    for i in range(number_of_boxes):
        top_left_corner = (int(image[4*i+2]),int(image[4*i+3]))
        bottom_right_corner = (int(image[4*i+4]),int(image[4*i+5]))
        new_box = [top_left_corner, bottom_right_corner]
        boxes.append(new_box)
    return(boxes)

# La fonction suivante prend en argument une liste de la forme ['image_name', 'number_of_boxes', '12','32','654', '526', ...] et dessine dans l'image correspondante les boîtes englobantes.
def draw_boxes(image):
        name = image[0]
        source_img = Image.open(os.path.join(THIS_FOLDER, 'images_' + name)).convert("RGBA")
        draw = ImageDraw.Draw(source_img)
        boxes = create_boxes(image)
        for box in boxes:
            top_left_corner = box[0]
            bottom_right_corner = box[1]
            draw.rectangle((top_left_corner, bottom_right_corner), fill=None, outline = "yellow")
        source_img.show()

# La fonction suivante est une simple itération de la précédente permettant de dessiner toutes les boîtes englobantes sur toutes les images données dans le fichier texte 'annotations'.
def final_draw():
    for image in images_boxes:
        draw_boxes(image)

final_draw()
